class Dimenzije(object):
    """klasa dimenzije"""
    def __init__(self, duzina, sirina, visina):
        self.duzina = duzina
        self.sirina = sirina
        self.visina = visina
    
    @property
    def duzina(self):
        return self.__duzina
        
    @duzina.setter
    def duzina(self, value):
        if value <= 0:
            raise ValueError("duzina mora biti veca od nule ")
        self.__duzina = value
        
    @property
    def sirina(self):
        return self.__sirina
        
    @sirina.setter
    def sirina(self, value):
        if value <= 0:
            raise ValueError("sirina mora biti veca od nule ")
        self.__sirina = value
    @property
    def visina(self):
        return self.__visina
        
    @visina.setter
    def visina(self, value):
        if value <= 0:
            raise ValueError("visina mora biti veca od nule ")
        self.__visina = value
    
    def __str__(self):
        return "{} {} {}".format(self.duzina, self.sirina, self.visina)

